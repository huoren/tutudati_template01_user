import request from "@/utils/request"

export function templateSubscribe(params) {
	return request.post("message/template/subscribe", params).then(res => {
		return res
	})
}