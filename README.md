## 项目说明

用户端使用的是uniapp开发，UI框架是使用的图鸟UI。

## 依赖包安装

由于node的包非常大，所以提供源代码的时候就不提供第三方的依赖包，需要自己安装。在项目目录下执行`npm i`即可安装。

## 项目配置

1. 域名配置，打开utils目录下的request.js文件，按照文件说明配置实际的API地址。
2. App.vue里面的examUpdateTemplateId改成自己的微信小程序订阅模板id。
3. 代码中有涉及到开发者的二维码图片，根据不同的地方替换成自己即可。
4. 小程序配置，找到manifest.json文件，将下面的appid改成你自己的小程序appid。
```javascript
"mp-weixin": {
    "appid": "wxd9f531131711833d",
    "setting": {
        "urlCheck": false,
        "minified": true,
        "postcss": true,
        "es6": true
    },
    "usingComponents": true,
    "LazyCodeLoading": true,
    "__usePrivacyCheck__": true
},
```
1. uniapp开发引用配置。找到manifest.json文件，替换成自己实际的域名信息。
```javascrit
"name": "项目的名称，可以改成成小程序的名称",
"appid": "uniapp上创建一个用，会分配一个引用appid，填写到此处",
"description": "项目描述，也可以写成小程序的名称或者描述",
```

## 官方客服

欢迎你在使用兔兔答题的过程中，遇到任何问题都可以添加官方客服和官方技术进行支持，也欢迎你一起参交流。

![](./image/guanfangkefu.jpeg)

## 仓库地址

1、[后端开源地址](https://gitee.com/shyjfang_admin/tutudati_template01_api)

2、[前端开源地址](https://gitee.com/shyjfang_admin/tutudati_template01_user)

3、[文档地址](https://wiki.tutudati.com/doc/56/)

## 预览图

![](https://imgcdn.tutudati.com/tutudatitemplate001.jpeg)

![](https://imgcdn.tutudati.com/tutudatitemplate002.jpeg)

![](https://imgcdn.tutudati.com/tutudatitemplate003.jpeg)

![](https://imgcdn.tutudati.com/tutudatitemplate004.jpeg)